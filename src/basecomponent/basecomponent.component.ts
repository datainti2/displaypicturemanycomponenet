import { Component, OnInit,Output, Input, EventEmitter, ViewContainerRef, ViewEncapsulation  } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-basecomponent',
  templateUrl: './basecomponent.component.html',
  styleUrls: ['./basecomponent.component.css']
})
export class BasecomponentComponent implements OnInit {
 private image;
  private title;
 private content;
  private items: any;

  constructor() {

  }
  ngOnInit() {
    this.items = 

    [
        {
            "image": "assets/foto1.jpg",
            "title":"Air enting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi",
            "content":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi."
           
        },
        {
            "image": "assets/foto2.jpg",
            "title":"Api",
             "content":"API (Application Programming Interface) adalah sekumpulan perintah, fungsi, komponen, dan protokol yang disediakan oleh sistem operasi ataupun bahasa pemrograman tertentu yang dapat digunakan oleh programmer saat membangun perangkat lunak"
           
        },
         {
            "image": "assets/foto3.jpg",
            "title":"Udara",
          "content":"Udara adalah suatu campuran gas yang terdapat pada lapisan yang mengelilingi bumi."
        },
         {
            "image": "assets/foto4.jpg",
            "title":"Oksigen",
            "content":"Oksigen atau zat asam adalah unsur kimia dalam sistem tabel periodik yang mempunyai lambang O dan nomor atom 8. Ia merupakan unsur golongan kalkogen dan dapat dengan mudah bereaksi dengan hampir semua unsur lainnya (utamanya menjadi oksida). ... Gas oksigen diatomik mengisi 20,9% volume atmosfer bumi"
        },
         {
            "image": "assets/foto5.jpg",
            "title":"kehidupan",
           "content":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi.",
           
        },
    ];
     this.onSelect(this.items[0]);
  }

  //buat load data baru
  ngOnChanges() {
    this.items = [];
  }

  onSelect(subitem) {
    if (subitem) {
      this.image = subitem.image;
      this.title = subitem.title;
      this.content = subitem.content;

    }
  }
}
